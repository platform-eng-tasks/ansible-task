## Ansible Task

**Repo directory structure**
```
├── README.md
├── inventory      -- contains hosts information
│   └── hosts
├── nice-script.sh -- script file which needed for skeleton dir
└── playbook.yml   -- main ansible playbook
```

***NOTE:***
    In this task I'm using **docker container as remote machine**. So follow the below prerequisite steps 

1. Make sure you have docker runtime installed on your PC
2. Start Ubuntu container in detached mode with sleep flag to avoid exiting container.
```bash
docker run -d --name mylinux ubuntu sh -c "sleep 60000"
```
3. Then run the below command to install required binaries for our tasks \
```bash
docker exec mylinux sh -c "apt update && apt install python3 -y && apt install sudo -y && apt install wget -y && apt install gpg -y && apt install lsb-release -y"
```
4. Thats it, our container ready to act as remote machine.

**Running Ansible playbook:**
> Run the below command to execute the ansible playbook:
```bash
ansible-playbook --inventory inventory/hosts playbook.yml
```

**validation:**
> get inside to our remote machine(mylinux container) to perform our validations
```bash
docker exec -it mylinux sh
```
> Check the skeleton dir. you should see the **nice-script.sh** file
```bash
$ ls -lart /etc/skel
-rw-r--r-- 1 root root  807 Jan  6  2022 .profile
-rw-r--r-- 1 root root 3771 Jan  6  2022 .bashrc
-rw-r--r-- 1 root root  220 Jan  6  2022 .bash_logout
-rwxr-xr-x 1 root root   20 Mar 27 17:26 nice-script.sh
drwxr-xr-x 1 root root 4096 Mar 27 17:26 .
drwxr-xr-x 1 root root 4096 Mar 27 17:29 ..
```
> Switch to newly created user(**john**). you should able to switch user without any issue
```bash
$ su john
$ whoami
john
```
> Check the user's(**john**) home directory (**/better-place/john**)
```bash
$ cd /better-place/john
$ ls -lart
total 24
-rw-r--r-- 1 john john  807 Jan  6  2022 .profile
-rw-r--r-- 1 john john 3771 Jan  6  2022 .bashrc
-rw-r--r-- 1 john john  220 Jan  6  2022 .bash_logout
-rwxr-xr-x 1 john john   20 Mar 27 17:26 nice-script.sh
drwxr-xr-x 2 john john 4096 Mar 27 17:26 .
drwxr-xr-x 3 root root 4096 Mar 27 17:26 ..
```
> Check the passwordless sudo access for **whoami** command and make sure if you use sudo for other commands it should ask password.
```bash
$ sudo whoami
root
$ sudo apt update
[sudo] password for john:
```
> Check the **tmux*** and ***vim** package installation
```bash
$ which tmux
/usr/bin/tmux
$ which vim
/usr/bin/vim
```
> Verify Terraform CLI installation
```bash
$ terraform -version
Terraform v1.7.5
on linux_amd64
```
